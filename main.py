#!/usr/bin/env python

import logging
import sys
import time
from argparse import ArgumentParser

from dms_to_dd.parse import parse_excel

if __name__ == '__main__':
    start_time = time.time()
    parser = ArgumentParser(description='Parse one or more columns in an excel spreadsheet formatted as '
                                        'degree-minute-second (DMS) into decimal degrees.',
                            epilog='For more information: https://gitlab.com/nasumilu1')

    parser.add_argument('-s', '--sheet', dest='sheet_name', metavar='SHEET_NAME',
                        help='Excel spreadsheet to parse.')

    parser.add_argument('-f', '--file', dest='in_excel', required=True, metavar='IN_FILE',
                        help='The path to the excel spreadsheet')

    parser.add_argument('-c', '--columns', dest='parse_columns', required=True, metavar='COLUMNS',
                        type=lambda x: x.split(','), help='The columns in the excel spreadsheet to parse.')

    parser.add_argument('-p', '--prefix', dest='column_prefix', metavar='PREFIX',
                        help='The prefix used to name the parsed column(s).')

    parser.add_argument('-o', '--output', dest='out_excel', metavar='OUT_FILE')

    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', help='Verbose output')
    args = parser.parse_args()

    logger = logging.getLogger('dms_to_dd')
    logger.setLevel(logging.DEBUG if args.verbose else logging.WARN)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    handler.setFormatter(logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        datefmt="%Y-%m-%d %H:%M:%S")
    )
    logger.addHandler(handler)
    try:
        parse_excel(
            args.in_excel,
            args.parse_columns,
            args.sheet_name or 0,
            args.column_prefix or 'parse_',
            args.out_excel
        )
    except ValueError as e:
        logger.error(
            'Unable to parse the excel spreadsheet, {in_excel}. Error: {e}'.format(in_excel=args.in_excel, e=e)
        )
        sys.exit(-1)
    logger.debug('Complete: {}ms '.format(round((time.time() - start_time)*1000, 3)))
    sys.exit(0)
