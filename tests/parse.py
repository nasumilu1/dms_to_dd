import unittest

from dms_to_dd.parse import parse_dms


class DMSParseTest(unittest.TestCase):

    def test_dms_parse_north_and_east(self) -> None:
        expected = 30.563889
        inputs = ['30°33’50"N', '30°33’50”E', '30°33\'50"N', '30°33\'50”E']
        values = [parse_dms(value) for value in inputs]
        for value in values:
            self.assertEqual(expected, value)

    def test_dms_parse_south_and_west(self) -> None:
        expected = -30.563889
        inputs = ['30°33’50"S', '30°33’50”S', '30°33\'50"W', '30°33\'50”W']
        values = [parse_dms(value) for value in inputs]
        for value in values:
            self.assertEqual(expected, value)
