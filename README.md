# Decimal-Minutes-Seconds to Decimal Degree Parser

Simple python script which parses a decimal-minutes-seconds to decimal degree value.

## Basic Usage

```python
from dms_to_dd.parse import parse_dms

dms = '30°33’50”N'
print(parse_dms(dms))

# Expected Output:
# 30.563889
```

or to 1000ths 

```python
from dms_to_dd.parse import parse_dms

dms = '30°33’50”N'
print(parse_dms(dms))

# Expected Output:
# 30.564
```

## Parsing Excel Sheet

```python
from dms_to_dd.parse import parse_excel

file = '/home/user/data.xlsx'
parse_columns = ['lat_dms', 'lng_dms']

parse_excel(file, parse_columns)
# Output is written to the same xlsx file and sheet
```

Or to specify a different output file:

```python
from dms_to_dd.parse import parse_excel

file = '/home/user/data.xlsx'
parse_columns = ['lat_dms', 'lng_dms']
out_file = '/home/user/out-data.xlsx'

parse_excel(file, parse_columns, out_excel=out_file)
# Output is written to the file /home/user/out-data.xlsx
```

