import logging
import re
from typing import List
import pandas
import pandas as pd

logger = logging.getLogger('dms_to_dd')
pattern = re.compile((
    r'(?P<degree>\d{1,3}\u00b0)'  # match degrees
    r'|(?P<minutes>\d{0,2}[\u2019\u0027])'  # match minutes
    r'|(?P<seconds>\d{0,2}[\u201d\u0022])'  # match seconds
    r'|(?P<direction>[NESW])'  # match cardinal direction
), flags=re.IGNORECASE)


def parse_excel(in_excel: str,
                parse_columns: str | List[str],
                sheet_name: str | int | None = 0,
                column_prefix: str = 'parse_',
                out_excel: str | None = None) -> pd.DataFrame:
    """
    Parse specified columns in an Excel file and save the results in a new Excel file.

    :param in_excel: The path to the input Excel file.
    :param parse_columns: The columns to parse. Can be a single column name or a list of column names.
    :param sheet_name: The name or index of the sheet in the Excel file. Default is the first sheet (0).
    :param column_prefix: The prefix to add to the parsed columns in the output Excel file. Default is 'parse_'.
    :param out_excel: The path to the output Excel file. Default is None, which means the input Excel file will be overwritten.
    :return: A pandas DataFrame containing the parsed columns.

    :raises ValueError: If the specified sheet name is invalid.
    """
    xls = pandas.ExcelFile(in_excel)

    sheet_names = xls.sheet_names

    _sheet_name = sheet_name if isinstance(sheet_name, str) else sheet_names[sheet_name]
    logger.debug('Parsing Excel file: {} using sheet name {}'.format(in_excel, _sheet_name))
    if _sheet_name not in sheet_names:
        raise ValueError('Invalid sheet: {}'.format(_sheet_name))

    parse_columns = [parse_columns] if isinstance(parse_columns, str) else parse_columns

    df = pd.read_excel(xls, _sheet_name)
    for column in parse_columns:
        try:
            df[column_prefix + column] = df[column].apply(parse_str)
            logger.debug('Parsed column: {}'.format(column))
        except KeyError:
            raise ValueError('Invalid column: {}'.format(column))

    out_excel = in_excel if out_excel is None else out_excel
    df.to_excel(out_excel, sheet_name=_sheet_name, index=False)
    logger.debug('Saved to Excel file: {}'.format(out_excel))
    return df


def parse_str(value: str, precision=6) -> float:
    """
    :param value: A string representing a coordinate value in Degree-Minute-Second (DMS) format. The value should
    contain degrees, minutes, seconds, and the cardinal direction (N, E, S,
    * or W).

    :param precision: An optional integer specifying the desired precision of the resulting decimal coordinate. The
    default is 6.

    :return: A float representing the parsed coordinate value in decimal degrees.

    :raises RuntimeError: If the DMS value is invalid and cannot be parsed.
    """
    matches = re.finditer(pattern, value)
    match = dict([(value.lastgroup, value[value.lastgroup]) for value in matches])
    try:
        degree = float(match['degree'][:-1])
        minutes = float(match['minutes'][:-1]) / 60
        seconds = float(match['seconds'][:-1]) / 3600
        direction = 1 if match['direction'] == 'N' or match['direction'] == 'E' else -1
    except KeyError as e:
        raise ValueError(f"Invalid DMS value: {e} is not found!")

    dd = round(direction * (degree + minutes + seconds), precision)

    if (match['direction'] == 'N' or match['direction'] == 'S') and (dd < -90 or dd > 90):
        raise ValueError('Invalid DMS value must be between -90 and 90 inclusive, found {}!'.format(value))
    elif dd < -180 or dd > 180:
        raise ValueError('Invalid DMS value must be between -180 and 180 inclusive, found {}!'.format(value))

    logger.debug('Converted decimal-minutes-seconds {} to decimal-degrees {}'.format(value, dd))
    return dd
